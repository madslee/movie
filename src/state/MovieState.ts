import React from 'react'
import globalHook, { Store } from 'use-global-hook';
import { Movie } from '../api/Movie';
import { MovieApi } from '../api/MovieApi';
import { DetailedMovie } from '../api/DetailedMovie';

let movieApi = new MovieApi()

export type MovieState = {
    movies: Movie[]
    searchString: string
    detailedMovie: DetailedMovie | null
}

export type MovieActions = {
    getPopularMovies: () => void
    searchMovie: (searchString: string) => void
    getImageThumbnailBaseUrl: () => string
    getDetailedMovie: (id: number) => void
}

const getPopularMovies = (store: Store<MovieState, MovieActions>) => {
    movieApi.getPopularMovies().then(movies => {
        store.setState({ ...store.state, movies: movies})
    })
}

const searchMovie = (store: Store<MovieState, MovieActions>, searchString: string) => {
    movieApi.searchMovie(searchString).then(movies => {
        store.setState({ ...store.state, movies: movies})
    })
    store.setState({ ...store.state, searchString: searchString})
}

const getImageThumbnailBaseUrl = (store: Store<MovieState, MovieActions>) => {
    return movieApi.getImageThumbnailBaseUrl()
}

const getDetailedMovie = (store: Store<MovieState, MovieActions>, id: number) => {
    store.setState({ ...store.state, detailedMovie: null})

    movieApi.getDetailedMovie(id).then(detailedMovie => {
        store.setState({ ...store.state, detailedMovie: detailedMovie})
    })
}

const initialState: MovieState = {
    searchString: "",
    movies: [],
    detailedMovie: null
}

const actions = {
    getPopularMovies,
    searchMovie,
    getImageThumbnailBaseUrl,
    getDetailedMovie
};

export const useMovieGlobal = globalHook<MovieState, MovieActions>(
    React,
    initialState,
    actions
);
