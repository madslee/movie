export interface DetailedMovie {
    id: number
    adult: boolean 
    backdrop_path: string 
    poster_path: string
    budget: number 
    genres: Genre[]
    imdb_id: string
    original_language: string 
    original_title: string 
    overview: string
    popularity: number
    belongs_to_collection: number 
    production_companies: ProductionCompany[]
    production_countries: ProductionCountry[]
    release_date: string 
    revenue: number 
    runtime: number 
    spoken_languages: Language[]
    status: string 
    tagline: string 
    title: string
    video: boolean
    vote_average: number 
    vote_count: number
}

export interface Genre {
    id: number
    name: string
}

export interface ProductionCompany {
    id: number
    logo_path: string 
    name: string
    origin_country: string
}

export interface ProductionCountry {
    iso_3166_1: string 
    name: string
}

export interface Language {
    iso_639_1: string 
    name: string 
}
