export interface Movie {
    id: number
    title: string
    original_title: string
    popularity: number
    vote_count: number
    vote_average: number
    release_date: string
    original_language: string
    genre_ids: number[]
    overview: string 
    backdrop_path: string 
    poster_path: string
}