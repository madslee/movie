import axios from 'axios'
import { Movie } from './Movie'
import { DetailedMovie } from './DetailedMovie'

export class MovieApi {

    private static apiKey = process.env.REACT_APP_MOVIE_API_KEY
    private baseUrl = "https://api.themoviedb.org/3/"

    getImageThumbnailBaseUrl(): string {
        return "https://image.tmdb.org/t/p/w500/"
    }

    async getPopularMovies(): Promise<Movie[]> {
        let url = "https://api.themoviedb.org/3/movie/popular?api_key=" + MovieApi.apiKey + "&language=en-US&page=1"
        const promise = await axios.get(url)

        return promise.data.results.map((jsonObject: Object) => {
            return jsonObject as Movie
        })
    }

    async searchMovie(searchString: string): Promise<Movie[]> {
        let url = this.baseUrl + "search/movie?api_key=" + MovieApi.apiKey + "&language=en-US&query=" + searchString + "&page=1&include_adult=false"
        const promise = await axios.get(url)

        return promise.data.results.map((jsonObject: Object) => {
            return jsonObject as Movie
        })
    }

    async getDetailedMovie(id: number): Promise<DetailedMovie> {
        let url = " https://api.themoviedb.org/3/movie/" + id + "?api_key=" + MovieApi.apiKey + "&language=en-US"
        const promise = await axios.get(url)
        return promise.data as DetailedMovie
    }
}