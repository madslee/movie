import React from 'react'
import "./header.scss"
import { Link } from 'react-router-dom'

function Header() {
    return <div className="header">
        <div className="container">
            <Link to={"/"}>
                <div className="logo">MoviesApp</div>
            </Link>
        </div>
    </div>
}

export default Header