import React, { useState, ChangeEvent } from "react"
import { useMovieGlobal } from "../state/MovieState"
import "./search.scss"

function Search() {

    const [state, actions] = useMovieGlobal();
    const [searchString, setSearchString] = useState("")
    const onSearchStringChange = (e: ChangeEvent<HTMLInputElement>) => setSearchString(e.target.value)

    return <div className="jumbotron movie-search">
        <div className="container">
            <input type="text" className="input input-md" id="searchStringInput" name="searchStringInput" onChange={onSearchStringChange}/>
            <button className="button-secondary button-md" onClick={() => actions.searchMovie(searchString)}>Go!</button>
        </div>
    </div>
}

export default Search;