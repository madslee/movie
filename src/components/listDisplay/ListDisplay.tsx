import React from "react"
import { listenerCount } from "process"
import "./listDisplay.scss"

type ListDisplayProps = {
    title: string 
    list: string[]
}

function ListDisplay(props: ListDisplayProps) {

    return <div>
        <h4>{props.title}</h4>
        <ul>
            {props.list.map((element, index) => {
                return <li>{element}</li>
            })}
        </ul>
    </div>

}

export default ListDisplay