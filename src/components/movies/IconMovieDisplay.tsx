import React from "react"

type IconMoviePropItem = {
    fontAwesomeStyle: string 
    value: string
}

type IconMoviePropList = {
    list: IconMoviePropItem[]
}

function IconMovieDisplay(props: IconMoviePropList) {
    return <div className="icon-value-list">
        {props.list.map((prop, index) =>{
            return <div>
                  <i className={prop.fontAwesomeStyle}></i><span className="highlight">{prop.value}</span>
            </div>
        })}
    </div>
}

export default IconMovieDisplay