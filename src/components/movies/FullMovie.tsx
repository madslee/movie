import React, { useState, useEffect } from "react"
import { useMovieGlobal } from "../../state/MovieState";
import ListDisplay from "../listDisplay/ListDisplay";
import IconMovieDisplay from "./IconMovieDisplay";

function FullMovie() {

    const [state, actions] = useMovieGlobal();
    const [isLoading, setIsLoading] = useState(true);
  
    useEffect(() => {
        if (isLoading) {
            let movieId = window.location.href.split("/").splice(-1).pop()
            actions.getDetailedMovie(parseInt(movieId!!))
            setIsLoading(false)
        }
    }, [isLoading, actions]);

    function getSpokenLanguages(): string[] {
        let arr = state.detailedMovie?.spoken_languages.map((lang) => {
            return lang.name
        })
        return arr === undefined ?  [] : arr
    }

    function getGenres(): string[] {
        let arr = state.detailedMovie?.genres.map((genre) => {
            return genre.name
        })
        return arr === undefined ? [] : arr
    }

    function getProductionCountries(): string[] {
        let arr = state.detailedMovie?.production_countries.map((country) => {
            return country.name
        })
        return arr === undefined ? [] : arr
    }

    function getBudgetString(): string {
        let num = state.detailedMovie?.budget
        if (num === undefined || num < 1000) return ""

        let numString = num.toString()
        let newNumString = ""

        for (var i = numString.length - 1; i >= 0; i--) {
            if ( (i - 1) % 3 === 0) {
                newNumString = " " + newNumString
            }
            newNumString = numString[i] + newNumString
        }
        return newNumString
    }

    return <div>
        {state.detailedMovie !== null &&
            <div className="container">
                <div className="movie-full-page-column">
                    <div className="image-wrapper">
                        <img src={actions.getImageThumbnailBaseUrl() + state.detailedMovie.poster_path}></img>
                    </div>
                    <div className="description">
                        <h1>{state.detailedMovie.title}</h1>
                        <h3 className="light muted">{state.detailedMovie.tagline}</h3>
                        <div>{state.detailedMovie.overview}</div>

                        <div className="primary-info-container">
                            <div><b>IMDB rating:</b> {state.detailedMovie.vote_average}</div>
                            <div><b>Runtime: </b> {state.detailedMovie.runtime} minutes</div> 
                            <div><b>Released:</b> {state.detailedMovie.release_date.substring(0, 4)}</div>
                        </div>


                        <div className="info-lists-container">
                            <ListDisplay title="Languages" list={getSpokenLanguages()} />
                            <ListDisplay title="Genres" list={getGenres()} />
                            <ListDisplay title="Countries" list={getProductionCountries()} />
                        </div>
                    </div>
                </div>
            </div>
        }
    </div>
}

export default FullMovie