import React from "react"
import { useMovieGlobal } from "../../state/MovieState";
import "./movie.scss"
import MovieCard from "./MovieCard";

function MovieList() {

    const [state, actions] = useMovieGlobal();

    return <div className="row row-space-between">
        {state.movies.map((movie, index) => {
            return <div className="movie-column">
                <MovieCard movie={movie} movieImageBaseUrl={actions.getImageThumbnailBaseUrl()} linkWhenClicked={"/id/" + movie.id}/>
            </div>
        })}
    </div>
}

export default MovieList