import React from "react"
import { Movie } from "../../api/Movie"
import { Link } from "react-router-dom"
import IconMovieDisplay from "./IconMovieDisplay"

type MovieCardProps = {
    movie: Movie
    movieImageBaseUrl: string
    linkWhenClicked: string | null
}

function MovieCard(props: MovieCardProps) {

    return <Link to={"/id/" + props.movie.id}>
        <div className="movie-card">
            <img src={props.movieImageBaseUrl + props.movie.poster_path}></img>

            <div className="description">
                <h2>{props.movie.title}</h2>
                <p>{props.movie.overview}</p>
                <IconMovieDisplay list={[ 
                    {fontAwesomeStyle: "far fa-star", value: props.movie.vote_average.toString()},
                    {fontAwesomeStyle: "far fa-calendar-alt", value: props.movie.release_date.substring(0, 4)},
                    {fontAwesomeStyle: "fas fa-language", value: props.movie.original_language.toUpperCase()}
                 ]} />
            </div>
        </div>
    </Link>

}

export default MovieCard