import React, { useState, useEffect } from 'react';
import "./style.scss"
import "./form.scss"
import Header from './components/Header';
import Search from './components/Search';
import MovieList from './components/movies/MovieList';
import { useMovieGlobal } from './state/MovieState';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'
import FullMovie from './components/movies/FullMovie';

function App() {

  const [state, actions] = useMovieGlobal();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
      if (isLoading) {
          actions.getPopularMovies()
          setIsLoading(false)
      }
  }, [isLoading, actions]);

  function renderFrontPage() {
    return (
      <div>
        <Header/>
        <Search/>
        <div className="container">
          <MovieList/>
        </div>
      </div>
    );
  }

  function renderMoviePage() {
    return (
      <div>
        <Header/>
        <Search/>
        <FullMovie/>
      </div>
    );
  }

  
  return <Router>
    <Switch>
      <Route path="/id/*">
        {renderMoviePage()}
      </Route>

      <Route path="/">
        {renderFrontPage()}
      </Route> 
    </Switch>
  </Router>
}

export default App;
